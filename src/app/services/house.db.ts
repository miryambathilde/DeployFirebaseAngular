import { NumberFormatStyle } from "@angular/common";
import { House } from '../interfaces/house.interface';

export const HOUSES: House[] = [
  {
    id: 1,
    titulo: 'Bonita casa adosada',
    direccion: 'Calle del Almendro 17, 28090',
    ciudad: 'Madrid',
    numeroHabitaciones: 3,
    propietario: 'Ramiro',
    disponibilidad: true,
    foto:
      'https://images.unsplash.com/photo-1575517111478-7f6afd0973db?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTV8fGhvdXNlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
    lat: 1.1,
    long: 2.2
  },
  {
    id: 2,
    titulo: 'Atico de lujo en el centro',
    direccion: 'Calle Gran via 17, 28013',
    ciudad: 'Madrid',
    numeroHabitaciones: 3,
    propietario: 'Stephan',
    disponibilidad: false,
    foto:
      'https://storage.googleapis.com/static.inmoweb.es/clients/258/property/234918/image/1224_11429811152.jpg?v=20150428173403800',
    lat: 1.1,
    long: 2.2
  },
  {
    id: 3,
    titulo: 'Piso duplex en Barcelona',
    direccion: 'Avenida de la Ramblas 23, 08028',
    ciudad: 'Barcelona',
    numeroHabitaciones: 4,
    propietario: 'Miryam',
    disponibilidad: true,
    foto:
      'https://www.smartrentalcollection.com/content/imgsxml/galerias/panel_infoapartamentos/1/duplex4dormitorios01898.jpg',
    lat: 1.1,
    long: 2.2
  },
  {
    id: 4,
    titulo: 'Chalet con vista al Mar',
    direccion: 'Calle de la Rosa 27',
    ciudad: 'Valencia',
    numeroHabitaciones: 7,
    propietario: 'Rosa',
    disponibilidad: false,
    foto: 'https://www.cw-casaibiza.es/upload/116759.jpg',
    lat: 1.1,
    long: 2.2
  }
];
