import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { House } from 'src/app/interfaces/house.interface';
import { HousesService } from 'src/app/services/houses.service';

@Component({
  selector: 'app-house-details',
  templateUrl: './house-details.component.html',
  styleUrls: ['./house-details.component.css']
})
export class HouseDetailsComponent implements OnInit {
  myHouse: House | undefined;

  constructor(private activedRouted: ActivatedRoute, private housesService: HousesService) { }

  ngOnInit(): void {
    this.activedRouted.params.subscribe(params => {
      const result = this.housesService.getHouseById(params.idhouse);
      result.subscribe(data => {
        this.myHouse = data;
        console.log(this.myHouse);
      });
    });
  }
}
