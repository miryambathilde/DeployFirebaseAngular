import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { HousesService } from "src/app/services/houses.service";


@Component({
  selector: 'app-new-house',
  templateUrl: './new-house.component.html',
  styleUrls: ['./new-house.component.css']
})
export class NewHouseComponent implements OnInit {

  houseUpdated : any = {};

  constructor(private router: Router, private housesService: HousesService, private activedRouted: ActivatedRoute) { }

  ngOnInit(): void {
    this.activedRouted.params.subscribe(params => {
      if (params.idhouse) {
        //Puedo preguntar por los datos de la casa por id
        const response = this.housesService.getHouseById(params.idhouse);
        response.subscribe(data => {
          this.houseUpdated = data
        })
      }
    })
  }

  async onSubmit(pForm: any) {
    const casa = pForm.value;
    casa.disponibilidad = true;
    let message: any;
    if (this.houseUpdated.id) {
      message = await this.housesService.create(casa, this.houseUpdated.id);
    } else {
      message = await this.housesService.create(casa);
    }

    console.log(message);
    if (message.success) {
      this.router.navigate(['/home'])
    }
    //console.log(pForm);
  }

}
