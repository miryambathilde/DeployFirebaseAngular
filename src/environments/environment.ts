// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBTG5xGzJZMEXKpZsTziJYuGhefUe5sr8w",
    authDomain: "airbb-fb0d1.firebaseapp.com",
    projectId: "airbb-fb0d1",
    storageBucket: "airbb-fb0d1.appspot.com",
    messagingSenderId: "313635394221",
    appId: "1:313635394221:web:22f59ebbaedf4b4126ec24"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
