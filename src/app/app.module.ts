import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HouseDetailsComponent } from './components/house-details/house-details.component';
import { NewHouseComponent } from './components/new-house/new-house.component';
import { CardHouseComponent } from './components/card-house/card-house.component';
import { FormsModule } from "@angular/forms";
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from "src/environments/environment";
import { AgmCoreModule } from "@agm/core";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HouseDetailsComponent,
    NewHouseComponent,
    CardHouseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC4xb9kb314LfNC-y3WEv6y89H1sX1D_wk' // api key de google cloud google maps
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
