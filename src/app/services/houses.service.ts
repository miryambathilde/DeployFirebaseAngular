import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/compat/firestore";
import { Observable } from "rxjs";
import { House } from "../interfaces/house.interface";
import { map } from "rxjs/operators";
//import { HOUSES } from "./house.db";

@Injectable({
  providedIn: 'root'
})
export class HousesService {
  //arrHouses: House[] = [];
  collection!: AngularFirestoreCollection<House>;

  constructor(private firestore: AngularFirestore) {
    this.collection = this.firestore.collection<House>('houses');
    //console.log(this.collection)
    //this.arrHouses = HOUSES;
  }

  getAll(): Observable<any> {
    return this.collection.valueChanges();
  }

  getHouseById(pId: string): Observable<any> {
    return this.collection.doc(pId).valueChanges();
  }

  delete(pId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.collection.doc(pId).delete();
        resolve({ success: 'ok'});
      } catch (error) {
        reject(error);
      }
    });
  }

  //crear o actualizar //
  create(pHouse: House, pId: string = ""): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        //resolve, recibo una house pero sin id, firestore tiene un metodo que me permite crear un id y
        //asignarselo al nuevo elemento(ID de documento)
        //creo el id
        const id = (pId ==="") ? this.firestore.createId() : pId;
        // se lo asigno a pHouse
        pHouse.id = id;
        // se lo añado a pHouse
        const result = this.collection.doc(id).set(pHouse);
        resolve({ success: 'vivienda añadida' });
      } catch (error) {
        //reject
        reject(error);
      }
    });
  }
}
