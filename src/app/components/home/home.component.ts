import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { House } from 'src/app/interfaces/house.interface';
import { HousesService } from 'src/app/services/houses.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  arrHouses: Observable<any[]> = new Observable();
  latitud: number = 0;
  longitud: number = 0;

  constructor(private housesService: HousesService) {
    navigator.geolocation.getCurrentPosition(position => {
      this.latitud = position.coords.latitude;
      this.longitud = position.coords.longitude;
      console.log(position);
    });
  }

  ngOnInit(): void {
    this.arrHouses = this.housesService.getAll();
    //this.arrHouses.subscribe(data => console.log(data));
  }

  onRightClick($event: any) {
    alert('tocando boton derecho');
    console.log($event);
  }
}
